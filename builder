#!/bin/bash

if [[ -z "${CI_REGISTRY_PASSWORD}" ]]; then
    podman login -u "${CI_REGISTRY_USER}" "${CI_REGISTRY}"
fi

container=$(buildah from centos:"${CENTOS_VERSION}")
buildah run "${container}"  /bin/sh << EOF
yum install -y scl-utils bash-completion

yum clean all
rm -fr /var/cache/*
EOF
buildah config --author 'Jonathan MERCIER aka bioinfornatics' "${container}"
buildah config --comment 'Minimal Image to use SCLs' "${container}"
buildah config --cmd /bin/bash "${container}"

echo "${container}"
